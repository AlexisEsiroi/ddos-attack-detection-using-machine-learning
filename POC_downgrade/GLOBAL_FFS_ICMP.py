import math
from statistics import variance
from threading import Timer
import pyshark
import schedule
import pickle
import time
import sys 
import pandas as pd
import datetime

# RANDOM FOREST

# 'Destination_Port' VALIDÉ
# 'Flow_Duration' ? regarder le flag de la connexion TCP quand le flag est sur fin -> stop de timer
# 'Total_Fwd_Packets'
# 'Total_Backward_Packets'
# 'Total_Length_of_Fwd_Packets'
# 'Total_Length_of_Bwd_Packets'
# 'Fwd_Packet_Length_Max'
# 'Flow_Bytes_Sec'
# 'Flow_Packets_Sec'
# 'Flow_IAT_Mean'




# https://github.com/ahlashkari/CICFlowMeter/blob/master/ReadMe.txt 

my_ip_address = "10.167.114.118"
tab_features = ["Destination_Port","Flow_Duration","Total_Fwd_Packets","Total_Backward_Packets","Total_Length_of_Fwd_Packets","Total_Length_of_Bwd_Packets","Fwd_Packet_Length_Max","Flow_Bytes_Sec""Flow_Packets_Sec","Flow_IAT_Mean"]
dataset = {
    "timestamp" : [],
    "Destination_Port" : [],
    "Flow_Duration" : [],
    "Total_Fwd_Packets" : [],
    "Total_Backward_Packets" : [],
    "Total_Length_of_Fwd_Packets" : [],
    "Total_Length_of_Bwd_Packets" : [],
    "Fwd_Packet_Length_Max" : [],
    "Flow_Bytes_Sec" : [],
    "Flow_Packets_Sec" : [],
    "Flow_IAT_Mean" : [],
    "Label" : [],
    "predicted_Label" : [],
}
compteur = 0
protocol = ""
Destination_Port = ""
flowIpAddress = []
flowInformation = {}
flowPacketTab = {}
## TCP FLAG
FIN = "0x001"
FIN_ACK = "0x011"

Flow_IAT_Max = 0
Bwd_Packets_Sec = 0
Fwd_IAT_Max = 0

def calculSum(values):
    sum = 0
    for value in values:
        sum += value
    return(sum)

def calculMoyenne(values):
    if len(values) != 0:
        return(calculSum(values) / len(values))

def metrics():
    global flowInformation
    global flowPacketTab
    global protocol
    global nbFeatures
    for addr in flowIpAddress:
        metrics = [flowInformation[addr]["Destination_Port"],flowInformation[addr]["Flow_Duration"],flowInformation[addr]["Total_Fwd_Packets"],flowInformation[addr]["Total_Backward_Packets"],flowInformation[addr]["Total_Length_of_Fwd_Packets"],flowInformation[addr]["Total_Length_of_Bwd_Packets"],flowInformation[addr]["Fwd_Packet_Length_Max"],flowInformation[addr]["Flow_Bytes_Sec"],flowInformation[addr]["Flow_Packets_Sec"],calculMoyenne(flowInformation[addr]["Flow_IAT_Mean"])]
        predict = loaded_model.predict([metrics[:nbFeatures]])
        print("------- Protocol : ",protocol," ----------")
        print("Source IP address --> ",addr)
        print("Destination IP address --> ",my_ip_address)
        for feature in tab_features[:nbFeatures]:
            print(feature," --> ",flowInformation[addr][feature])
        print("PREDICTION --> ",predict)

        ## Dataset Calcul Accuracy
        now = datetime.datetime.now()
        date = datetime.datetime(now.year,now.month,now.day,now.hour,now.minute,now.second)
        dataset["timestamp"].append(date)
        cpt = 0
        for feature in tab_features[:nbFeatures]:
            print("## Remplissage du dataset")
            dataset[feature].append(metrics[cpt])
            cpt+=1
        dataset["Label"].append("BENIGN")
        dataset["predicted_Label"].append(predict[0])
        
        
        if flowInformation[addr]["connection_sate"]:
            ## Delete the IP address
            del flowInformation[addr]
            del flowIpAddress[flowIpAddress.index(addr)]


print("### Forward Feature Selection method ###")
cyberAttack = str(input("Entrez le type d'attaque :"))
nbFeatures = int(input("Entrez le nombre de feature pour la detection :"))
model_file = './Feature selection/Models_trained/RF_FFS_'+str(nbFeatures)+'.sav'
loaded_model = pickle.load(open(model_file, 'rb'))
schedule.every(1).seconds.do(metrics)

# load the model from disk

iface_name = 'eth0'
filter_string = 'http'
capture = pyshark.LiveCapture(
    interface=iface_name,
)
try:
    for packet in capture.sniff_continuously():
        try:
            schedule.run_pending()
            protocol=packet.highest_layer
            if(protocol!="ICMP"):
                protocolTransport = packet.transport_layer
            else:
                protocolTransport = "aucun"
            src_address = packet.ip.src
            dst_address = packet.ip.dst

            ## identification des flows 
            if(src_address not in flowIpAddress):
                first_time = time.time()
                flowIpAddress.append(src_address)
                flowInformation[src_address] = {
                    "Destination_Port" : 0,
                    "Flow_Duration" : 0, 
                    "first_time" : first_time,
                    "connection_sate" : False,
                    "Total_Fwd_Packets" : 0, 
                    "Total_Backward_Packets" : 0, 
                    "Total_Length_of_Fwd_Packets" : 0, 
                    "Total_Length_of_Bwd_Packets" : 0, 
                    "Fwd_Packet_Length_Max" : 0, 
                    "Flow_Bytes_Sec" : 0, 
                    "Flow_Packets_Sec" : 1,
                    "Flow_IAT_Mean" : [], 
                }
                flowPacketTab[src_address] = []
                flowPacketTab[src_address].append(int(packet.captured_length))
            else:
                flowPacketTab[src_address].append(int(packet.captured_length))

            ## destination port
            if(protocolTransport != "aucun"):
                Destination_Port = packet[protocolTransport].srcport
            else:
                Destination_Port = "0"

            for addr in flowIpAddress:
                if(addr == src_address or addr == dst_address):
                    flowInformation[addr]["Flow_Packets_Sec"]+=1
                    if (protocol!="ICMP"):
                        flowInformation[addr]["Flow_Bytes_Sec"]+=int(packet.captured_length)
                    else:
                        flowInformation[addr]["Flow_Bytes_Sec"]+=int(packet[protocol].data_len)


                    flowInformation[addr]["Destination_Port"]=Destination_Port
                    
                    ## Flow_IAT_Mean
                    if (protocol!="ICMP"):
                        flowInformation[addr]["Flow_IAT_Mean"].append(int(packet.captured_length))
                    else:
                        flowInformation[addr]["Flow_IAT_Mean"].append(int(packet[protocol].data_len))
                    
                    ## Flow_Duration
                    flowInformation[addr]["Flow_Duration"] = time.time() - flowInformation[addr]["first_time"]

                    ## Connection state
                    if (protocol!="ICMP"):
                        if packet[protocolTransport].flags == FIN or packet[protocolTransport].flags == FIN_ACK:
                            flowInformation[addr]["connection_sate"] = True

                ## Forward and src_address ==  my_ip_address
                if (addr == dst_address ):
                    ## Fwd_Packet_Length_Max
                    if (protocol!="ICMP"):
                        if(int(packet.captured_length)>flowInformation[addr]["Fwd_Packet_Length_Max"]):
                            flowInformation[addr]["Fwd_Packet_Length_Max"] = int(packet.captured_length)
                    else:
                        if(int(packet[protocol].data_len)>flowInformation[addr]["Fwd_Packet_Length_Max"]):
                            flowInformation[addr]["Fwd_Packet_Length_Max"] = int(packet[protocol].data_len)


                    ## Total_Fwd_Packets
                    flowInformation[addr]["Total_Fwd_Packets"]+=1

                    if (protocol!="ICMP"):
                        ## Total_Length_of_Fwd_Packets
                        flowInformation[addr]["Total_Length_of_Fwd_Packets"]+=int(packet.captured_length)
                    else:
                        flowInformation[addr]["Total_Length_of_Fwd_Packets"]+=int(packet[protocol].data_len)

                    
                ## Backward  and dst_address == my_ip_address
                if (addr == src_address):

                    ## Total_Backward_Packets
                    flowInformation[addr]["Total_Backward_Packets"]+=1

                    ## Total_Length_of_Bwd_Packets
                    if (protocol!="ICMP"):
                        flowInformation[addr]["Total_Length_of_Bwd_Packets"]+=int(packet.captured_length)
                    else:
                        flowInformation[addr]["Total_Length_of_Bwd_Packets"]+=int(packet[protocol].data_len)

        except: 
            pass
except KeyboardInterrupt:
    ## export csv
    print ('Exportation du dataset en CSV')
    ntab = ["timestamp"] + tab_features[:nbFeatures] + ["Label","predicted_Label"]
    newDataSet = {}
    for column in ntab:
        newDataSet[column] = dataset[column]
    print(newDataSet)
    pd.DataFrame(newDataSet).to_csv("./Accuracy_model/"+cyberAttack+"_data_FFS-"+str(nbFeatures)+".csv",index=False)
    sys.exit("AH OUI OUI OUI")

# test