
# svmem(total=8589934592, available=2473299968, 
# percent=71.2, used=4373254144, free=109592576, 
# active=2358906880, inactive=2353942528, wired=2014347264)

import datetime
from time import sleep
import psutil
import pandas as pd

dataset = {
    "CPU" : [],
    "MEMORY" : [],
    "TS" : [],
    "time" : [],
}
debut = datetime.datetime.now()
flag = True
cyberAttack = str(input("Entrez le type d'attaque :"))
while flag:
    try:
        cpu_percent = psutil.cpu_percent(interval=1)
        memory_percent = psutil.virtual_memory().percent
        timestamp = datetime.datetime.now()
        time = (timestamp-debut).total_seconds()
        dataset["CPU"].append(cpu_percent)
        dataset["MEMORY"].append(memory_percent)
        dataset["TS"].append(timestamp)        
        dataset["time"].append(time)        
        print("-------- DATA -----------")
        print("TS -> ",timestamp-debut)
        print("Time -> ",time)
        print("CPU -> ",cpu_percent)
        print("Memory -> ",memory_percent)
        sleep(1)
    except KeyboardInterrupt:
        print ('Exportation du dataset en CSV')
        pd.DataFrame(dataset).to_csv("./data/data_"+cyberAttack+".csv")
        flag = False
