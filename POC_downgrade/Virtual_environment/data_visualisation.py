import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as md
import os

compteur=0
cyber_attacks = ["ICMP", "SYN", "saphyra","hulk","akira","botnet","httpdoser","slowloris","sadboys","sadattack","torshammer"]
metric_measured = ["CPU","MEMORY"]
compteur=0
for attack in cyber_attacks:
    path = "./data/data_"+attack+".csv"
    if os.path.isfile(path):
        dataset = pd.read_csv(path)
        for metric in metric_measured:
            print("--------",metric,"--------")
            plt.figure(compteur)
            print(dataset["TS"])
            plt.plot(dataset["time"],dataset[metric],label=metric)
            plt.grid(True)
            plt.xlabel("Seconds")
            plt.ylabel(metric+" percent")
            plt.legend()
            plt.title(metric+" usage")
            chemin = "./results/"+attack+"_"+metric+".png"
            print(chemin)
            plt.savefig(chemin)
            compteur+=1
