#!/bin/sh

# network tools
sudo apt install net-tools

# git
sudo apt install git

# pip command
sudo apt install python3-pip

# Librairies
pip install pyshark
pip install schedule
pip install -U scikit-learn
pip install pandas
pip install DateTime

# Node js
sudo apt install curl
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs