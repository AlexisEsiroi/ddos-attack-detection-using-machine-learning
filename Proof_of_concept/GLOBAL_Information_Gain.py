import math
from statistics import variance
from threading import Timer
import pyshark
import schedule
import pickle
import time
import sys 
import pandas as pd
import datetime

# RANDOM FOREST

# 'Destination_Port' VALIDÉ
# 'Flow_Duration' ? regarder le flag de la connexion TCP quand le flag est sur fin -> stop de timer
# 'Total_Fwd_Packets'
# 'Total_Backward_Packets'
# 'Total_Length_of_Fwd_Packets'
# 'Total_Length_of_Bwd_Packets'
# 'Fwd_Packet_Length_Max'
# 'Flow_Bytes_Sec'
# 'Flow_Packets_Sec'
# 'Flow_IAT_Mean'




# https://github.com/ahlashkari/CICFlowMeter/blob/master/ReadMe.txt 


compteur = 0
protocol = ""
Destination_Port = ""
Max_Packet_Length = 0
Fwd_Packet_Length_Max = 0
src_address = ""
dst_address = ""
flowIpAddress = []
flowInformation = {}
flowPacketTab = {}

my_ip_address = "10.167.114.118"
tab_features = ["Flow_Bytes_Sec","Flow_Packets_Sec","Destination_Port","Max_Packet_Length","Fwd_Packet_Length_Max","Packet_Length_Variance","Packet_Length_Std","Bwd_Packets_Sec","Flow_IAT_Max","Fwd_IAT_Max"]
dataset = {
    "timestamp" : [],
    "Flow_Bytes_Sec" : [],
    "Flow_Packets_Sec" : [],
    "Destination_Port" : [],
    "Max_Packet_Length" : [],
    "Fwd_Packet_Length_Max" : [],
    "Packet_Length_Variance" : [],
    "Packet_Length_Std" : [],
    "Bwd_Packets_Sec" : [],
    "Flow_IAT_Max" : [],
    "Fwd_IAT_Max" : [],
    "Label" : [],
    "predicted_Label" : [],
}
compteur = 0
protocol = ""
Destination_Port = ""
flowIpAddress = []
flowInformation = {}
flowPacketTab = {}
## TCP FLAG
FIN = "0x001"
FIN_ACK = "0x011"

Flow_IAT_Max = 0
Bwd_Packets_Sec = 0
Fwd_IAT_Max = 0

def calculSum(values):
    sum = 0
    for value in values:
        sum += value
    return(sum)

def calculMoyenne(values):
    if len(values) != 0:
        return(calculSum(values) / len(values))

def calculVariance(values):
    if len(values) != 0:
        sumVariance = 0
        variance = 0
        moy = calculMoyenne(values)
        for value in values:
            sumVariance += (value - moy) ** 2
        variance = sumVariance / len(values)
        return(variance)

def calculStd(values):
    if len(values) > 1:
        sumVariance = 0
        std = 0
        moy = calculMoyenne(values)
        for value in values:
            sumVariance += (value - moy) ** 2
        std = sumVariance / (len(values)-1)
        return(math.sqrt(std))
    elif len(values) == 1:
        return(values[0])
    else:
        return 0

def metrics():
    global flowInformation
    global flowPacketTab
    global protocol
    global nbFeatures
    for addr in flowIpAddress:
        print("dans la boucle")
        metrics = [flowInformation[addr]["Flow_Bytes_Sec"],flowInformation[addr]["Flow_Packets_Sec"],flowInformation[addr]["Destination_Port"],flowInformation[addr]["Max_Packet_Length"],flowInformation[addr]["Fwd_Packet_Length_Max"],calculVariance(flowPacketTab[src_address]),calculStd(flowPacketTab[src_address]),flowInformation[src_address]["Bwd_Packets_Sec"],flowInformation[src_address]["Flow_IAT_Max"],flowInformation[src_address]["Fwd_IAT_Max"]]
        predict = loaded_model.predict([metrics[:nbFeatures]])
        print("après la boucle ouais")
        print("------- Protocol : ",protocol," ----------")
        print("Source IP address --> ",addr)
        print("Destination IP address --> ",my_ip_address)
        for feature in tab_features[:nbFeatures]:
            print(feature," --> ",flowInformation[addr][feature])
        print("PREDICTION --> ",predict)

        ## Dataset Calcul Accuracy
        now = datetime.datetime.now()
        date = datetime.datetime(now.year,now.month,now.day,now.hour,now.minute,now.second)
        dataset["timestamp"].append(date)
        cpt = 0
        for feature in tab_features[:nbFeatures]:
            print("## Remplissage du dataset")
            dataset[feature].append(metrics[cpt])
            cpt+=1
        dataset["Label"].append("BENIGN")
        dataset["predicted_Label"].append(predict[0])
        
        if flowInformation[addr]["connection_sate"]:
            ## Delete the IP address
            del flowInformation[addr]
            del flowIpAddress[flowIpAddress.index(addr)]


print("### Information Gain method ###")
cyberAttack = str(input("Entrez le type d'attaque :"))
nbFeatures = int(input("Entrez le nombre de feature pour la detection :"))
model_file = './Feature selection/Models_trained/RF_InfGain_'+str(nbFeatures)+'.sav'
loaded_model = pickle.load(open(model_file, 'rb'))
schedule.every(1).seconds.do(metrics)

# load the model from disk

iface_name = 'enp0s3'
filter_string = 'http'
capture = pyshark.LiveCapture(
    interface=iface_name,
)
try:
    for packet in capture.sniff_continuously():
        try:
            schedule.run_pending()
            protocolTransport = packet.transport_layer
            protocol=packet.highest_layer
            src_address = packet.ip.src
            dst_address = packet.ip.dst

            ## identification des flows 
            if(src_address not in flowIpAddress):
                flowIpAddress.append(src_address)
                flowInformation[src_address] = {
                    "Flow_Packets_Sec" : 1,
                    "Flow_Bytes_Sec" : int(packet.captured_length), 
                    "connection_sate" : False,
                    "Destination_Port" : 0, 
                    "Max_Packet_Length" : 0, 
                    "Fwd_Packet_Length_Max" : 0, 
                    "Packet_Length_Variance" : 0, 
                    "Packet_Length_Std" : 0, 
                    "Bwd_Packets_Sec" : 0, 
                    "Flow_IAT_Max" : 0,
                    "Fwd_IAT_Max" : 0, 
                }
                flowPacketTab[src_address] = []
                flowPacketTab[src_address].append(int(packet.captured_length))
            else:
                flowPacketTab[src_address].append(int(packet.captured_length))

            ## destination port
            if(protocolTransport):
                Destination_Port = packet[protocolTransport].srcport
            else:
                Destination_Port = "N/A"

            for addr in flowIpAddress:
                if(addr == src_address or addr == dst_address):
                    flowInformation[addr]["Flow_Packets_Sec"]+=1
                    flowInformation[addr]["Flow_Bytes_Sec"]+=int(packet.captured_length)
                    flowInformation[addr]["Destination_Port"]=Destination_Port
                    ## Max length packet
                    if(int(packet.captured_length)>flowInformation[addr]["Max_Packet_Length"]):
                        flowInformation[addr]["Max_Packet_Length"] = int(packet.captured_length)
                    
                    ## Flow_IAT_Max
                    if(float(packet[protocolTransport].time_delta)>flowInformation[addr]["Flow_IAT_Max"]):
                        flowInformation[addr]["Flow_IAT_Max"] = int(packet.captured_length)
                    
                    ## Connection state
                    if packet[protocolTransport].flags == FIN or packet[protocolTransport].flags == FIN_ACK:
                        flowInformation[addr]["connection_sate"] = True

                ## Forward 
                if (addr == dst_address and src_address ==  "10.167.114.118"):                
                    ## Fwd_Packet_Length_Max
                    if(int(packet.captured_length)>flowInformation[addr]["Fwd_Packet_Length_Max"]):
                        flowInformation[addr]["Fwd_Packet_Length_Max"] = int(packet.captured_length)
                    
                    ## Fwd_IAT_Max
                    if(float(packet[protocolTransport].time_delta)>flowInformation[addr]["Fwd_IAT_Max"]):
                        flowInformation[addr]["Fwd_IAT_Max"] = float(packet[protocolTransport].time_delta)

                    
                ## Backward 
                if (addr == src_address and dst_address == "10.167.114.118"):
                    flowInformation[addr]["Bwd_Packets_Sec"]+=1
        except: 
            pass
except KeyboardInterrupt:
    ## export csv
    print ('Exportation du dataset en CSV')
    ntab = ["timestamp"] + tab_features[:nbFeatures] + ["Label","predicted_Label"]
    newDataSet = {}
    for column in ntab:
        newDataSet[column] = dataset[column]
    print(newDataSet)
    pd.DataFrame(newDataSet).to_csv("./Accuracy_model/"+cyberAttack+"_data_InfGain-"+str(nbFeatures)+".csv",index=False)
    sys.exit("AH OUI OUI OUI")