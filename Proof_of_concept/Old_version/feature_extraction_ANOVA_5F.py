import math
from statistics import variance
import pyshark
import pandas as pd
import sys 
import schedule
import pickle
import datetime
import time




# https://github.com/ahlashkari/CICFlowMeter/blob/master/ReadMe.txt 

# Flow_Packets_Sec VALIDE
# SYN_Flag_Count VALIDE
# Flow_Bytes_Sec VALIDE
# Min_Packet_Length VALIDE 
# Fwd_IAT_Std (VALIDE)
# ---------------------
# SYN = "0x002"
# SYN_ACK = "0x012"
# SYN_ACK_ENC = "0x052"
# SYN_ACK_CWR = "0x0c2"
# ---------------------
# Bwd_Packet_Length_Min, VALIDE
# Flow_IAT_Max VALIDE
# Fwd_IAT_Max VALIDE
# Flow_IAT_Std VALIDE

compteur = 0
protocol = ""
Destination_Port = ""
Max_Packet_Length = 0
Fwd_Packet_Length_Max = 0
src_address = ""
dst_address = ""
flowIpAddress = []
flowInformation = {}
flowPacketTab = {}
flowIAT = {}
flowIATstd = {}

## TCP FLAG
FIN = "0x001"
FIN_ACK = "0x0011"
SYN = "0x002"
ACK = "0x0010"
SYN_ACK = "0x0012"
SYN_ACK_ENC = "0x0052"
SYN_ACK_CWR = "0x00c2"
tab_syn = [SYN,SYN_ACK,SYN_ACK_ENC,SYN_ACK_CWR]

# load the model from disk
model_file = './Feature selection/Models_trained/RF_ANOVA_5F.sav'
loaded_model = pickle.load(open(model_file, 'rb'))

dataset = {
    "timestamp" : [],
    "Flow_Packets_Sec" : [],
    "SYN_Flag_Count" : [],
    "Flow_Bytes_Sec" : [],
    "Min_Packet_Length" : [],
    "Fwd_IAT_Std" : [],
    "Label" : [],
    "predicted_Label" : [],
}

Flow_IAT_Max = 0
Bwd_Packets_Sec = 0
Fwd_IAT_Max = 0

def calculSum(values):
    sum = 0
    for value in values:
        sum += value
    return(sum)

def calculMoyenne(values):
    if len(values) != 0:
        return(calculSum(values) / len(values))

def calculVariance(values):
    if len(values) != 0:
        sumVariance = 0
        variance = 0
        moy = calculMoyenne(values)
        for value in values:
            sumVariance += (value - moy) ** 2
        variance = sumVariance / len(values)
        return(variance)

def calculStd(values):
    if len(values) > 1:
        sumVariance = 0
        std = 0
        moy = calculMoyenne(values)
        for value in values:
            sumVariance += (value - moy) ** 2
        std = sumVariance / (len(values)-1)
        return(math.sqrt(std))
    elif len(values) == 1:
        return(values[0])
    else:
        return 0

def metrics():
    global flowInformation
    global flowPacketTab
    global protocol
    for addr in flowIpAddress:
        predict = loaded_model.predict([[flowInformation[addr]["Flow_Packets_Sec"],flowInformation[addr]["SYN_Flag_Count"],flowInformation[addr]["Flow_Bytes_Sec"],flowInformation[addr]["Min_Packet_Length"],calculStd(flowIAT[src_address])]])
        print("------- Protocol : ",protocol," ----------")
        print("Source IP address --> ",addr)
        print("Destination IP address --> 10.167.114.118")
        print("Flow_Packets_Sec --> ",flowInformation[addr]["Flow_Packets_Sec"]," / s")
        print("SYN_Flag_Count --> ",flowInformation[addr]["SYN_Flag_Count"]) # reset (0)
        print("Flow_Bytes_Sec --> ",flowInformation[addr]["Flow_Bytes_Sec"]," bytes / s")
        print("Min_Packet_Length --> ",flowInformation[addr]["Min_Packet_Length"]," bytes")
        print("Fwd_IAT_Std --> ",calculStd(flowIAT[src_address])," seconds")
        print("Bwd_Packet_Length_Min --> ",flowInformation[addr]["Bwd_Packet_Length_Min"]," bytes")
        print("Flow_IAT_Max --> ", flowInformation[src_address]["Flow_IAT_Max"]," / s")
        print("Fwd_IAT_Max --> ", flowInformation[src_address]["Fwd_IAT_Max"]," / s")
        print("Flow_IAT_Std --> ",calculStd(flowIATstd[src_address])," / s")
        print("PREDICTION --> ",predict)

        ## Dataset Calcul Accuracy
        now = datetime.datetime.now()
        date = datetime.datetime(now.year,now.month,now.day,now.hour,now.minute,now.second)
        dataset["timestamp"].append(date)
        dataset["Flow_Packets_Sec"].append(flowInformation[addr]["Flow_Packets_Sec"])
        dataset["SYN_Flag_Count"].append(flowInformation[addr]["SYN_Flag_Count"])
        dataset["Flow_Bytes_Sec"].append(flowInformation[addr]["Flow_Bytes_Sec"])
        dataset["Min_Packet_Length"].append(flowInformation[addr]["Min_Packet_Length"])
        dataset["Fwd_IAT_Std"].append(flowInformation[addr]["Fwd_IAT_Std"])
        dataset["Label"].append("BENIGN")
        dataset["predicted_Label"].append(predict[0])

        # A voir lequel on reset
        flowInformation[addr]["Flow_Packets_Sec"] = 0
        flowInformation[addr]["SYN_Flag_Count"] = 0
        flowInformation[addr]["Flow_Bytes_Sec"] = 0
        flowInformation[addr]["Min_Packet_Length"] = 1600
        flowInformation[addr]["Fwd_IAT_Std"] = 0
        flowInformation[addr]["Flow_IAT_Std"] = 0


        if flowInformation[addr]["connection_sate"]:
            ## Delete the IP address
            del flowInformation[addr]
            del flowIpAddress[flowIpAddress.index(addr)]

cyberAttack = str(input("Entrez le type d'attaque :"))
schedule.every(1).seconds.do(metrics)

# enp0s3
iface_name = 'enp0s3'
filter_string = 'http'
capture = pyshark.LiveCapture(
    interface=iface_name,
)
try:
    for packet in capture.sniff_continuously():
        try:
        # if packet != 0:
            schedule.run_pending()
            protocolTransport = packet.transport_layer
            protocol=packet.highest_layer
            src_address = packet.ip.src
            dst_address = packet.ip.dst

            ## identification des flows 
            if(src_address not in flowIpAddress):
                flowIpAddress.append(src_address)
                flowInformation[src_address] = {
                    "Flow_Packets_Sec" : 1,
                    "SYN_Flag_Count" : 0,
                    "Flow_Bytes_Sec" : int(packet.captured_length), 
                    "connection_sate" : False,
                    "Min_Packet_Length" : 1600, 
                    "Fwd_IAT_Std" : 0, 
                    "Bwd_Packet_Length_Min" : 1600, 
                    "Flow_IAT_Max" : 0, 
                    "Fwd_IAT_Max" : 0, 
                    "Flow_IAT_Std" : 0, 
                }
                flowIAT[src_address] = [] 
                flowIATstd[src_address] = []
                flowPacketTab[src_address] = []
                flowPacketTab[src_address].append(int(packet.captured_length))
            else:
                flowPacketTab[src_address].append(int(packet.captured_length))

            for addr in flowIpAddress:
                if(addr == src_address or addr == dst_address):
                    flowInformation[addr]["Flow_Packets_Sec"]+=1
                    flowInformation[addr]["Flow_Bytes_Sec"]+=int(packet.captured_length)
                    
                    ## Min_Packet_Length
                    if(int(packet.captured_length)<flowInformation[addr]["Min_Packet_Length"]):
                        flowInformation[addr]["Min_Packet_Length"] = int(packet.captured_length)
                        
                    ## Connection state
                    if packet[protocolTransport].flags == FIN or packet[protocolTransport].flags == FIN_ACK:
                        flowInformation[addr]["connection_sate"] = True

                    ## SYN_Flag_Count
                    for flag in tab_syn:
                        if packet[protocolTransport].flags == flag:
                            flowInformation[addr]["SYN_Flag_Count"]+=1

                    ## Flow_IAT_Max
                    if(float(packet[protocolTransport].time_delta)>flowInformation[addr]["Flow_IAT_Max"]):
                        flowInformation[addr]["Flow_IAT_Max"] = int(packet.captured_length)

                    ## Flow_IAT_Std
                    flowIATstd[src_address].append(float(packet[protocolTransport].time_delta))
                ## Forward 
                if (addr == dst_address and src_address ==  "10.167.114.118"):        
                    ## Fwd_IAT_Std  
                    flowIAT[src_address].append(float(packet[protocolTransport].time_delta))

                    ## Fwd_IAT_Max
                    if(float(packet[protocolTransport].time_delta)>flowInformation[addr]["Fwd_IAT_Max"]):
                        flowInformation[addr]["Fwd_IAT_Max"] = float(packet[protocolTransport].time_delta)


                ## Backward 
                if (addr == src_address and dst_address == "10.167.114.118"):

                    ## Bwd_Packet_Length_Min
                    if(int(packet.captured_length)<flowInformation[addr]["Bwd_Packet_Length_Min"]):
                        flowInformation[addr]["Bwd_Packet_Length_Min"] = int(packet.captured_length)
        except: 
            pass
         
except KeyboardInterrupt:
    ## export csv
    print ('Exportation du dataset en CSV')
    pd.DataFrame(dataset).to_csv("./Accuracy_model/"+cyberAttack+"_data_ANOVA-5F.csv",index=False)
    sys.exit("AH OUI OUI OUI")

    # print("------------Packet n°",Flow_Packets_Sec,"-------------")
    # print("Flow_Packets_Sec ")
    # print("Flow_Bytes_Sec ")
    # print("Max_Packet_Length ",packet.captured_length," bytes")
    # print("Fwd_Packet_Length_Max ")
    # print("Packet_Length_Std ")
    # print("Packet_Length_Variance ")
# print("Destination_Port ",packet[protocolTransport].dstport)  
    # print("Flow_IAT_Max ")
    # print("Bwd_Packets_Sec ")
    # print("Fwd_IAT_Max ")

    # print('Source IP address: ' + packet.ip.src)
    # print('destination IP address: ' + packet.ip.dst)
    # print("Source mac address: ",packet.eth.src)
    # print("Destination mac address: ",packet.eth.dst)
    # print("Time To Live: ",packet.ip.ttl)
    # print("Communication standard: ",protocolTransport)
    # print("Application layer protocol: ",protocol)
    # print("Destination port:",packet[protocolTransport].srcport)
    # print("Source port: ",packet[protocolTransport].dstport)
    # print("Packet size: ",packet.captured_length," bytes")
    # print("TCP Flag: ",packet[protocolTransport].flags.showname)
    # print("Time delta: ",packet[protocolTransport].time_delta) # temps depuis de dernier packet reçu (à voir si c'est important)
    # print("Time relative: ",packet[protocolTransport].time_delta) # voir ce que c'est je sais pas à quoi ça correspond