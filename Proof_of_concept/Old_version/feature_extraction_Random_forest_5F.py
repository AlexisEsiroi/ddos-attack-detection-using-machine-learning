import math
from statistics import variance
import pandas as pd
from threading import Timer
import pyshark
import schedule
import pickle
import time
import sys 
import datetime
# RANDOM FOREST

# 'Destination_Port' VALIDÉ
# 'Flow_Duration' ? regarder le flag de la connexion TCP quand le flag est sur fin -> stop de timer
# 'Total_Fwd_Packets'
# 'Total_Backward_Packets'
# 'Total_Length_of_Fwd_Packets'
# 'Total_Length_of_Bwd_Packets'
# 'Fwd_Packet_Length_Max'
# 'Flow_Bytes_Sec'
# 'Flow_Packets_Sec'
# 'Flow_IAT_Mean'




# https://github.com/ahlashkari/CICFlowMeter/blob/master/ReadMe.txt 

my_ip_address = "10.167.114.118"
dataset = {
    "timestamp" : [],
    "Destination_Port" : [],
    "Flow_Duration" : [],
    "Total_Fwd_Packets" : [],
    "Total_Backward_Packets" : [],
    "Total_Length_of_Fwd_Packets" : [],
    "Total_Length_of_Bwd_Packets" : [],
    "Label" : [],
    "predicted_Label" : [],
}
compteur = 0
protocol = ""
Destination_Port = ""
flowIpAddress = []
flowInformation = {}
flowPacketTab = {}
## TCP FLAG
FIN = "0x001"
FIN_ACK = "0x011"

Flow_IAT_Max = 0
Bwd_Packets_Sec = 0
Fwd_IAT_Max = 0

# load the model from disk
model_file = './Feature selection/Models_trained/RF_FFS_5F.sav'
loaded_model = pickle.load(open(model_file, 'rb'))

def calculSum(values):
    sum = 0
    for value in values:
        sum += value
    return(sum)

def calculMoyenne(values):
    if len(values) != 0:
        return(calculSum(values) / len(values))

def metrics():
    global flowInformation
    global flowPacketTab
    global protocol
    for addr in flowIpAddress:
        predict = loaded_model.predict([[flowInformation[addr]["Destination_Port"],flowInformation[addr]["Flow_Duration"],flowInformation[addr]["Total_Fwd_Packets"],flowInformation[addr]["Total_Backward_Packets"],flowInformation[addr]["Total_Length_of_Fwd_Packets"]]])
        print("------- Protocol : ",protocol," ----------")
        print("Source IP address --> ",addr)
        print("Destination IP address --> ",my_ip_address)
        print("Source_port (server side so destination port host side) ",flowInformation[addr]["Destination_Port"])  
        print("Flow_Duration --> ",flowInformation[addr]["Flow_Duration"]," seconds")
        print("Total_Fwd_Packets --> ",flowInformation[addr]["Total_Fwd_Packets"])
        print("Total_Backward_Packets --> ",flowInformation[addr]["Total_Backward_Packets"])
        print("Total_Length_of_Fwd_Packets --> ",flowInformation[addr]["Total_Length_of_Fwd_Packets"]," bytes")
        print("Total_Length_of_Bwd_Packets --> ",flowInformation[addr]["Total_Length_of_Bwd_Packets"]," bytes")
        print("Fwd_Packet_Length_Max --> ",flowInformation[addr]["Fwd_Packet_Length_Max"]," bytes")
        print("Flow_Bytes_Sec --> ",flowInformation[addr]["Flow_Bytes_Sec"]," bytes / s")
        print("Flow_Packets_Sec --> ",flowInformation[addr]["Flow_Packets_Sec"]," / s")
        print("Flow_IAT_Mean --> ",calculMoyenne(flowInformation[addr]["Flow_IAT_Mean"])," s")
        print("PREDICTION --> ",predict)

        ## Dataset Calcul Accuracy
        now = datetime.datetime.now()
        date = datetime.datetime(now.year,now.month,now.day,now.hour,now.minute,now.second)
        dataset["timestamp"].append(date)
        dataset["Destination_Port"].append(flowInformation[addr]["Destination_Port"])
        dataset["Flow_Duration"].append(flowInformation[addr]["Flow_Duration"])
        dataset["Total_Fwd_Packets"].append(flowInformation[addr]["Total_Fwd_Packets"])
        dataset["Total_Backward_Packets"].append(flowInformation[addr]["Total_Backward_Packets"])
        dataset["Total_Length_of_Fwd_Packets"].append(flowInformation[addr]["Total_Length_of_Fwd_Packets"])
        dataset["Total_Length_of_Bwd_Packets"].append(flowInformation[addr]["Total_Length_of_Bwd_Packets"])
        dataset["Label"].append("BENIGN")
        dataset["predicted_Label"].append(predict[0])

        if flowInformation[addr]["connection_sate"]:
            ## Delete the IP address
            del flowInformation[addr]
            del flowIpAddress[flowIpAddress.index(addr)]


cyberAttack = str(input("Entrez le type d'attaque :"))
schedule.every(1).seconds.do(metrics)

# enp0s3
iface_name = 'enp0s3'
filter_string = 'http'
capture = pyshark.LiveCapture(
    interface=iface_name,
)
try:
    for packet in capture.sniff_continuously():
        try:
            schedule.run_pending()
            protocolTransport = packet.transport_layer
            protocol=packet.highest_layer
            src_address = packet.ip.src
            dst_address = packet.ip.dst

            ## identification des flows 
            if(src_address not in flowIpAddress):
                first_time = time.time()
                flowIpAddress.append(src_address)
                flowInformation[src_address] = {
                    "Destination_Port" : 0,
                    "Flow_Duration" : 0, 
                    "first_time" : first_time,
                    "connection_sate" : False,
                    "Total_Fwd_Packets" : 0, 
                    "Total_Backward_Packets" : 0, 
                    "Total_Length_of_Fwd_Packets" : 0, 
                    "Total_Length_of_Bwd_Packets" : 0, 
                    "Fwd_Packet_Length_Max" : 0, 
                    "Flow_Bytes_Sec" : int(packet.captured_length), 
                    "Flow_Packets_Sec" : 1,
                    "Flow_IAT_Mean" : [], 
                }
                flowPacketTab[src_address] = []
                flowPacketTab[src_address].append(int(packet.captured_length))
            else:
                flowPacketTab[src_address].append(int(packet.captured_length))

            ## destination port
            if(protocolTransport):
                Destination_Port = packet[protocolTransport].srcport
            else:
                Destination_Port = "N/A"

            for addr in flowIpAddress:
                if(addr == src_address or addr == dst_address):
                    flowInformation[addr]["Flow_Packets_Sec"]+=1
                    flowInformation[addr]["Flow_Bytes_Sec"]+=int(packet.captured_length)
                    flowInformation[addr]["Destination_Port"]=Destination_Port
                    
                    ## Flow_IAT_Mean
                    flowInformation[addr]["Flow_IAT_Mean"].append(int(packet.captured_length))

                    ## Flow_Duration
                    flowInformation[addr]["Flow_Duration"] = time.time() - flowInformation[addr]["first_time"]

                    ## Connection state
                    if packet[protocolTransport].flags == FIN or packet[protocolTransport].flags == FIN_ACK:
                        flowInformation[addr]["connection_sate"] = True

                ## Forward 
                if (addr == dst_address):
                    
                    ## Fwd_Packet_Length_Max
                    if(int(packet.captured_length)>flowInformation[addr]["Fwd_Packet_Length_Max"]):
                        flowInformation[addr]["Fwd_Packet_Length_Max"] = int(packet.captured_length)

                    ## Total_Fwd_Packets
                    flowInformation[addr]["Total_Fwd_Packets"]+=1

                    ## Total_Length_of_Fwd_Packets
                    flowInformation[addr]["Total_Length_of_Fwd_Packets"]+=int(packet.captured_length)

                    
                ## Backward 
                if (addr == src_address):

                    ## Total_Backward_Packets
                    flowInformation[addr]["Total_Backward_Packets"]+=1

                    ## Total_Length_of_Bwd_Packets
                    flowInformation[addr]["Total_Length_of_Bwd_Packets"]+=int(packet.captured_length)
        except KeyboardInterrupt:
            sys.exit("AH OUI NON NON")
        except: 
            pass   
except KeyboardInterrupt:
    ## export csv
    print ('Exportation du dataset en CSV')
    pd.DataFrame(dataset).to_csv("./Accuracy_model/"+cyberAttack+"_data_FFS-5F.csv",index=False)
    sys.exit("AH OUI OUI OUI")