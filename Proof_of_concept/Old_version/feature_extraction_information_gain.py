import math
from statistics import variance
import pyshark
import schedule
import time
import pickle
import json
import pandas as pd
import sys 
import datetime



# https://github.com/ahlashkari/CICFlowMeter/blob/master/ReadMe.txt 

# Flow_Packets_Sec --> Number of flow packets per second  (VALIDÉ) 
# Flow_Bytes_Sec --> Number of flow bytes per seconds (VALIDÉ)
# Max_Packet_Length --> (VALIDÉ)
# Fwd_Packet_Length_Max --> Maximum size of packet in forward direction (VALIDÉ)
# Packet_Length_Std --> Standard deviation length of a packet (VALIDÉ)
# Packet_Length_Variance --> Variance length of a packet (VALIDÉ)
# Destination_Port (VALIDÉ)   
# Flow_IAT_Max --> Maximum time between two packets sent in the flow (VALIDE)          
# Bwd_Packets_Sec --> Number of backward packets per second (VALIDÉ)  
# Fwd_IAT_Max --> Maximum time between two packets sent in the forward direction (VALIDE)


compteur = 0
protocol = ""
Destination_Port = ""
Max_Packet_Length = 0
Fwd_Packet_Length_Max = 0
src_address = ""
dst_address = ""
flowIpAddress = []
flowInformation = {}
flowPacketTab = {}

dataset = {
    "timestamp" : [],
    "Flow_Bytes_Sec" : [],
    "Flow_Packets_Sec" : [],
    "Destination_Port" : [],
    "Max_Packet_Length" : [],
    "Fwd_Packet_Length_Max" : [],
    "Packet_Length_Variance" : [],
    "Packet_Length_Std" : [],
    "Bwd_Packets_Sec" : [],
    "Flow_IAT_Max" : [],
    "Fwd_IAT_Max" : [],
    "Label" : [],
    "predicted_Label" : [],
}

#Model

# load the model from disk
model_file = './Feature selection/Models_trained/RF_Information_gain.sav'
loaded_model = pickle.load(open(model_file, 'rb'))

## TCP FLAG
FIN = "0x001"
FIN_ACK = "0x011"


Flow_IAT_Max = 0
Bwd_Packets_Sec = 0
Fwd_IAT_Max = 0

def calculSum(values):
    sum = 0
    for value in values:
        sum += value
    return(sum)

def calculMoyenne(values):
    if len(values) != 0:
        return(calculSum(values) / len(values))

def calculVariance(values):
    if len(values) != 0:
        sumVariance = 0
        variance = 0
        moy = calculMoyenne(values)
        for value in values:
            sumVariance += (value - moy) ** 2
        variance = sumVariance / len(values)
        return(variance)

def calculStd(values):
    if len(values) > 1:
        sumVariance = 0
        std = 0
        moy = calculMoyenne(values)
        for value in values:
            sumVariance += (value - moy) ** 2
        std = sumVariance / (len(values)-1)
        return(math.sqrt(std))
    else:
        return(values[0])

def metrics():
    global flowInformation
    global flowPacketTab
    global protocol
    for addr in flowIpAddress:
        predict = loaded_model.predict([[flowInformation[addr]["Flow_Bytes_Sec"],flowInformation[addr]["Flow_Packets_Sec"],flowInformation[addr]["Destination_Port"],flowInformation[addr]["Max_Packet_Length"],flowInformation[addr]["Fwd_Packet_Length_Max"],calculVariance(flowPacketTab[src_address]),calculStd(flowPacketTab[src_address]),flowInformation[src_address]["Bwd_Packets_Sec"],flowInformation[src_address]["Flow_IAT_Max"],flowInformation[src_address]["Fwd_IAT_Max"]]])
        print("------- Protocol : ",protocol," ----------")
        print("Source IP address --> ",addr)
        print("Destination IP address --> 10.167.114.118")
        print("Flow_Bytes_Sec --> ",flowInformation[addr]["Flow_Bytes_Sec"]," bytes / s")
        print("Flow_Packets_Sec --> ",flowInformation[addr]["Flow_Packets_Sec"]," / s")
        print("Source_port (server side so destination port host side) ",flowInformation[addr]["Destination_Port"])  
        print("Max_Packet_Length --> ",flowInformation[addr]["Max_Packet_Length"]," bytes")
        print("Fwd_Packet_Length_Max --> ",flowInformation[addr]["Fwd_Packet_Length_Max"]," bytes")
        print("Packet_Length_Variance --> ",calculVariance(flowPacketTab[src_address]))
        print("Packet_Length_Std --> ",calculStd(flowPacketTab[src_address]))
        print("Bwd_Packets_Sec --> ", flowInformation[src_address]["Bwd_Packets_Sec"]," / s")
        print("Flow_IAT_Max --> ", flowInformation[src_address]["Flow_IAT_Max"]," / s")
        print("Fwd_IAT_Max --> ", flowInformation[src_address]["Fwd_IAT_Max"]," / s")
        print("PREDICTION --> ",predict)

        ## Dataset Calcul Accuracy
        now = datetime.datetime.now()
        date = datetime.datetime(now.year,now.month,now.day,now.hour,now.minute,now.second)
        dataset["timestamp"].append(date)
        dataset["Flow_Bytes_Sec"].append(flowInformation[addr]["Flow_Bytes_Sec"])
        dataset["Flow_Packets_Sec"].append(flowInformation[addr]["Flow_Packets_Sec"])
        dataset["Destination_Port"].append(flowInformation[addr]["Destination_Port"])
        dataset["Max_Packet_Length"].append(flowInformation[addr]["Max_Packet_Length"])
        dataset["Fwd_Packet_Length_Max"].append(flowInformation[addr]["Fwd_Packet_Length_Max"])
        dataset["Packet_Length_Variance"].append(calculVariance(flowPacketTab[src_address]))
        dataset["Packet_Length_Std"].append(calculStd(flowPacketTab[src_address]))
        dataset["Bwd_Packets_Sec"].append(flowInformation[addr]["Bwd_Packets_Sec"])
        dataset["Flow_IAT_Max"].append(flowInformation[addr]["Flow_IAT_Max"])
        dataset["Fwd_IAT_Max"].append(flowInformation[addr]["Fwd_IAT_Max"])
        dataset["Label"].append("BENIGN")
        dataset["predicted_Label"].append(predict[0])

        flowInformation[addr]["Flow_Packets_Sec"] = 0
        flowInformation[addr]["Flow_Bytes_Sec"] = 0
        flowInformation[addr]["Max_Packet_Length"] = 0
        flowInformation[addr]["Bwd_Packets_Sec"] = 0
        flowInformation[addr]["Fwd_Packet_Length_Max"] = 0
        if flowInformation[addr]["connection_sate"]:
            ## Delete the IP address
            del flowInformation[addr]
            del flowIpAddress[flowIpAddress.index(addr)]


cyberAttack = str(input("Entrez le type d'attaque :"))
schedule.every(1).seconds.do(metrics)

iface_name = 'enp0s3'
filter_string = 'http'
capture = pyshark.LiveCapture(
    interface=iface_name,
)
try:
    for packet in capture.sniff_continuously():
        try:
            schedule.run_pending()
            protocolTransport = packet.transport_layer
            protocol=packet.highest_layer
            src_address = packet.ip.src
            dst_address = packet.ip.dst

            ## identification des flows 
            if(src_address not in flowIpAddress):
                flowIpAddress.append(src_address)
                flowInformation[src_address] = {
                    "Flow_Packets_Sec" : 1,
                    "Flow_Bytes_Sec" : int(packet.captured_length), 
                    "connection_sate" : False,
                    "Destination_Port" : 0, 
                    "Max_Packet_Length" : 0, 
                    "Fwd_Packet_Length_Max" : 0, 
                    "Packet_Length_Variance" : 0, 
                    "Packet_Length_Std" : 0, 
                    "Bwd_Packets_Sec" : 0, 
                    "Flow_IAT_Max" : 0,
                    "Fwd_IAT_Max" : 0, 
                }
                flowPacketTab[src_address] = []
                flowPacketTab[src_address].append(int(packet.captured_length))
            else:
                flowPacketTab[src_address].append(int(packet.captured_length))

            ## destination port
            if(protocolTransport):
                Destination_Port = packet[protocolTransport].srcport
            else:
                Destination_Port = "N/A"

            for addr in flowIpAddress:
                if(addr == src_address or addr == dst_address):
                    flowInformation[addr]["Flow_Packets_Sec"]+=1
                    flowInformation[addr]["Flow_Bytes_Sec"]+=int(packet.captured_length)
                    flowInformation[addr]["Destination_Port"]=Destination_Port
                    ## Max length packet
                    if(int(packet.captured_length)>flowInformation[addr]["Max_Packet_Length"]):
                        flowInformation[addr]["Max_Packet_Length"] = int(packet.captured_length)
                    
                    ## Flow_IAT_Max
                    if(float(packet[protocolTransport].time_delta)>flowInformation[addr]["Flow_IAT_Max"]):
                        flowInformation[addr]["Flow_IAT_Max"] = int(packet.captured_length)
                    
                    ## Connection state
                    if packet[protocolTransport].flags == FIN or packet[protocolTransport].flags == FIN_ACK:
                        flowInformation[addr]["connection_sate"] = True

                ## Forward 
                if (addr == dst_address and src_address ==  "10.167.114.118"):                
                    ## Fwd_Packet_Length_Max
                    if(int(packet.captured_length)>flowInformation[addr]["Fwd_Packet_Length_Max"]):
                        flowInformation[addr]["Fwd_Packet_Length_Max"] = int(packet.captured_length)
                    
                    ## Fwd_IAT_Max
                    if(float(packet[protocolTransport].time_delta)>flowInformation[addr]["Fwd_IAT_Max"]):
                        flowInformation[addr]["Fwd_IAT_Max"] = float(packet[protocolTransport].time_delta)

                    
                ## Backward 
                if (addr == src_address and dst_address == "10.167.114.118"):
                    flowInformation[addr]["Bwd_Packets_Sec"]+=1
        except: 
            pass
except KeyboardInterrupt:
    ## export csv
    print ('Exportation du dataset en CSV')
    pd.DataFrame(dataset).to_csv("./Accuracy_model/"+cyberAttack+"_data_Information_gain-10F.csv",index=False)
    sys.exit("AH OUI OUI OUI")
         


    # print("------------Packet n°",Flow_Packets_Sec,"-------------")
    # print("Flow_Packets_Sec ")
    # print("Flow_Bytes_Sec ")
    # print("Max_Packet_Length ",packet.captured_length," bytes")
    # print("Fwd_Packet_Length_Max ")
    # print("Packet_Length_Std ")
    # print("Packet_Length_Variance ")
# print("Destination_Port ",packet[protocolTransport].dstport)  
    # print("Flow_IAT_Max ")
    # print("Bwd_Packets_Sec ")
    # print("Fwd_IAT_Max ")

    # print('Source IP address: ' + packet.ip.src)
    # print('destination IP address: ' + packet.ip.dst)
    # print("Source mac address: ",packet.eth.src)
    # print("Destination mac address: ",packet.eth.dst)
    # print("Time To Live: ",packet.ip.ttl)
    # print("Communication standard: ",protocolTransport)
    # print("Application layer protocol: ",protocol)
    # print("Destination port:",packet[protocolTransport].srcport)
    # print("Source port: ",packet[protocolTransport].dstport)
    # print("Packet size: ",packet.captured_length," bytes")
    # print("TCP Flag: ",packet[protocolTransport].flags.showname)
    # print("Time delta: ",packet[protocolTransport].time_delta) # temps depuis de dernier packet reçu (à voir si c'est important)
    # print("Time relative: ",packet[protocolTransport].time_delta) # voir ce que c'est je sais pas à quoi ça correspond