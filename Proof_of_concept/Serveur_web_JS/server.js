/* #region  Mise en place du serveur NodeJS */
let http = require('http');
let express = require('express');
let app = express();
let server = http.Server(app);
/* #endregion */

/* #region  Page index HTML automatique */
const debut_fichier = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Interface client v2.00</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="lib/jquery-3.6.0.slim.min.js"></script>
    <script src="/socket.io/socket.io.js"></script>
    <script src="js/io.js"></script>
    <script src="js/led.js"></script>
    </head>
    <body>
    <nav class="navbar navbar-dark sticky-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Welcome to the DDoS attack detection Proof of Concept</a>
        </div>
    </nav>
    <div class="container">
`;
const fin_fichier = `
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
`;
/* #endregion */

/* #region  Traitement d'une requête qui demande la page index.html */
app.all(["/", "/index.html"], (req, res) => {
    res.write(debut_fichier);
    for (let j = 0; j < 20; j++){
        res.write('<div class="row">')
        for (let i = 1; i < 5; i++) {
            res.write('<div class="col"><div class="card" style="width: 18rem;"><img src="images/img-'+i+'.png" class="card-img-top"><div class="card-body"><h5 class="card-title">Item n° '+i+'</h5><p class="card-text"> a brief description of the item '+i+' </p><a href="#" class="btn btn-outline-success">Purchase</a></div></div></div>')
        }
        res.write("</div>")
    }
    res.write(fin_fichier);
    res.end();
})
/* #endregion */

let server_io = require('socket.io');
let io_server = server_io(server);
let state_lum = [false,false,false]
io_server.on("connection", socket_client => {
    console.log('#### Un client se connecte');
    socket_client.on("disconnect",()=>{
        console.log('Un Client se déconnecte');
    })
})

app.use(express.static("./www"));
server.listen(80);
console.log("Serveur lancé !!!!!");
