import pandas as pd
from os import listdir
from datetime import timedelta
from os.path import isfile, join
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
import datetime
import json

# 0 - 2 --> Classico
# 2 - 3 --> DDoS
# 3 - 4 --> Classico

data = {}
attacks = ["HTTP","SYN","ICMP"]
methods = ["ANOVA","FFS","InfGain"]

for attack in attacks:
    data[attack] = {}
for attack in attacks:
    for method in methods:
        data[attack][method] = {}
for attack in attacks:
    for method in methods:
        for compteur in range(1,11):
            data[attack][method][compteur] = {}

for attack in attacks:
    for method in methods:
        for compteur in range(1,11):
            data[attack][method][compteur] = {
                "Accuracy" : 0,
                "Precision" : 0,
                "Recall" : 0,
                "TPR" : 0,
                "TNR" : 0,
                "FPR" : 0,
                "FNR" : 0,
            }
print(data)

files = [f for f in listdir("./Accuracy_model")
         if isfile(join("./Accuracy_model", f))]
files.sort()
for file in files:
    if(file != ".DS_Store" and file != "final_results.json" ):

        # HTTP_data_FFS-3.csv
        id = file.split("_")
        print(id)
        attack = id[0]
        id_feat = id[2].split("-")[1].split(".")[0]
        method =  id[2].split("-")[0]
        
        print("#### - ", method, " ",id_feat," - ",attack, " - ####")
        # set up le data frame
        dataset = pd.read_csv("./Accuracy_model/"+file)

        # set up les dates
        ref = datetime.datetime.strptime(
        dataset["timestamp"][0], "%Y-%m-%d %H:%M:%S")
        ddos_born_min = ref + timedelta(minutes=2)
        ddos_born_max = ref + timedelta(minutes=3)
        fin = ref + timedelta(minutes=4)

        # Set up les timesstamps
        print("## Experimental parameters ##")
        print("Date de debut --> ", ref)
        print("Debut DDoS --> ", ddos_born_min)
        print("Fin DDoS --> ", ddos_born_max)
        print("Date de fin --> ", fin)
        compteur = 0
        for value in dataset["timestamp"]:
            target = datetime.datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
            if(target > ddos_born_min and target < ddos_born_max):
                print("## Changement en DDoS")
                dataset["Label"][compteur] = "DDoS"
            compteur += 1
        print("## Calcul des métrics ##")
        accuracy = accuracy_score(dataset["Label"], dataset["predicted_Label"])
        precision = precision_score(dataset["Label"], dataset["predicted_Label"], average='macro')
        recall = recall_score(dataset["Label"], dataset["predicted_Label"], average='macro')
        matrix = confusion_matrix(dataset["Label"], dataset["predicted_Label"], labels=["DDoS", "BENIGN"])
        
        FP = matrix[0][0]
        FN = matrix[1][0]
        TP = matrix[1][1]
        TN = matrix[0][1]

        FP = FP.astype(float)
        FN = FN.astype(float)
        TP = TP.astype(float)
        TN = TN.astype(float)

        # Sensitivity, hit rate, recall, or true positive rate
        TPR = TP/(TP+FN)
        # Specificity or true negative rate
        TNR = TN/(TN+FP)
        # Fall out or false positive rate
        FPR = FP/(FP+TN)
        # False negative rate
        FNR = FN/(TP+FN)
        # Display of the metrics

        ## Data storage
        data[attack][method][int(id_feat)]["Accuracy"] = accuracy
        data[attack][method][int(id_feat)]["Precision"] = precision
        data[attack][method][int(id_feat)]["Recall"] = recall
        data[attack][method][int(id_feat)]["TPR"] = TPR
        data[attack][method][int(id_feat)]["TNR"] = TNR
        data[attack][method][int(id_feat)]["FPR"] = FPR
        data[attack][method][int(id_feat)]["FNR"] = FNR



        print("Accuracy du model --> ", accuracy)
        print("Precision du model --> ", precision)
        print("Recall du model --> ", recall)
        print("TPR --> ", TPR)
        print("TNR --> ", TNR)
        print("FPR --> ", FPR)
        print("FNR --> ", FNR ,"\n")

        path = "./Accuracy_model/"+file
        pd.DataFrame(dataset).to_csv(path, index=False)


print("### Exportation des données en JSON ----- <<>> ------")
with open('./Accuracy_model/final_results.json', 'w') as fp:
    json.dump(data, fp)
