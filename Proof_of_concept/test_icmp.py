import math
from statistics import variance
from threading import Timer
import pyshark
import schedule
import pickle
import time
import sys 
import pandas as pd
import datetime

iface_name = 'en0'
filter_string = 'http'
capture = pyshark.LiveCapture(
    interface=iface_name,
)

# 'Destination_Port' VALIDÉ
# 'Flow_Duration' ? regarder le flag de la connexion TCP quand le flag est sur fin -> stop de timer
# 'Total_Fwd_Packets'
# 'Total_Backward_Packets'
# 'Total_Length_of_Fwd_Packets'

# ['type', 'code', 'checksum', 'checksum_status', 'ident', 'ident_le', 'seq', 'seq_le', 'resp_to', 'resptime', 'data_time', 'data_time_relative', 'data', 'data_data', 'data_len']



for packet in capture.sniff_continuously():
    protocol=packet.highest_layer
    if protocol == "ICMP":
        print("---------",protocol,"-----------")
        print("data_time_relative ",packet[protocol].data_time_relative)
        print("data_len ",packet[protocol].data_len)
        # protocolTransport = packet[protocol].srcport
        # protocol=packet.highest_layer
        # src_address = packet.ip.src
        # dst_address = packet.ip.dst
        # print("protocolTransport --> ",protocolTransport)
        # print("protocol --> ",protocol)  
        # print("src_address --> ",src_address)
        # print("dst_address --> ",dst_address)

